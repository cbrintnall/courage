extends Node

const energy_per_turn = 3

# GLOBAL DATA ---

var energy = energy_per_turn

# ---

signal CardPlayed(card)
signal ItemPlaced(item)
signal WispLeft(wisp)
signal MoneyChanged(money)
signal NewCardInDeck(card)
signal NewTurn
signal EndTurn
