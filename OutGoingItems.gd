extends Node2D

onready var data = $"/root/Data"
onready var item_positions = [
	$Node2D,
	$Node2D2,
	$Node2D3
]

func _ready():
	$"/root/Events".connect("CardPlayed", self, "on_card_played")
	data.item_positions = self

func get_item(item:Dictionary):
	for item_position in item_positions:
		var contained_item = item_position.get_item()
		if contained_item and contained_item.get("name") == item["name"]: 
			return item_position
	return null

func item_is_placed(item: Dictionary):
	get_item(item) != null

func open_position() -> bool:
	for item_position in item_positions:
		if item_position.is_open():
			return true
	return false

func on_card_played(card):
	match card:
		{ "produces": var produces, .. }:
			for item_position in item_positions:
				# check if we have an open position, break if we have one
				if item_position.is_open():
					item_position.set_item(data.items[produces])
					break
