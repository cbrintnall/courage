extends Node2D

# SCENES ---

var _wisp := preload("res://Wisp.tscn")
var _card := preload("res://Card.tscn")

# ---

# PATHS ---

onready var line = $Line
onready var cards = $ui/MarginContainer/VBoxContainer/HBoxContainer
onready var data = $"/root/Data"

# END PATHS ---

var deck = []
var discard = []
var money = 0

# how many events to pull per turn
var events_per_turn = 3
var cards_per_turn = 3

func _ready():
	$"/root/Data".exit_point = $ExitPoint
	$"/root/Events".connect("WispLeft", self, "on_wisp_leave")
	$"/root/Events".connect("CardPlayed", self, "on_card_played")
	$"/root/Events".connect("EndTurn", self, "end_turn")
	
	call_deferred("_on_new_cycle")

func end_turn():
	for wisp in line.get_children():
		if wisp.has_method("leave"):
			wisp.leave()

func on_card_played(card):
	pass

func set_money():
	pass

func on_wisp_leave(_param):
	sync_wisp_line()
	
	# all wisps are served!
	if line.get_child_count() == 0:
		on_all_wisps_gone()
		
func on_all_wisps_gone():
	pass

func _deal_card():
	var chosen = data.possible_cards[randi()%len(data.possible_cards)].duplicate()
	Events.emit_signal("NewCardInDeck", chosen)

func _on_new_cycle():
	Events.emit_signal("NewTurn")
	
	for i in range(events_per_turn):
		var new_wisp = _wisp.instance()

		line.add_child(new_wisp)

		new_wisp.set_want(data.events[randi()%len(data.events)])
	
	# deals new cards
	for i in range(cards_per_turn):
		_deal_card()
		
	sync_wisp_line()

func sync_wisp_line():
	var line_length = line.curve.get_baked_length()
	
	var count = 0
	for child in line.get_children():
		if child is PathFollow2D:
			count += 1
			
			var target_position = clamp(
				line_length - (data.wisp_line_distance * (count-1)),
				0.0,
				line_length
			)
			
			child.move_to_offset(target_position, 0.0)
