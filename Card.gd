extends TextureRect

const offset = 7.5

var _data: Dictionary
var _entered := false

func _ready():
	connect("mouse_entered", self, "entered", [ true ])
	connect("mouse_exited", self, "entered", [ false ])
	
func _input(event):
	if _entered and event.is_action_pressed("ui_select"):
		play_card()

func can_play() -> bool:
	return (
		_data and
		("produces" in _data and $"/root/Data".item_positions.open_position())
	)

func play_card():
	if !can_play():
		card_play_rejected()
		return

	get_node("/root/Events").emit_signal("CardPlayed", _data)
	queue_free()

func card_play_rejected():
	pass

func entered(state: bool):
	_entered = state
	
	if state:
		rect_position += Vector2.UP * offset
	else:
		rect_position += Vector2.DOWN * offset

func set_card_data(data_map: Dictionary, data: Dictionary):
	_data = data
	
	var item = $item
	
	# look up in the data map what item this produces
	$item.texture.region = data_map[data["produces"]]["atlas_region"]
	$amount.text = str(data["amount"])
	$required_energy2.text = str(data["energy"])

