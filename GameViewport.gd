extends ViewportContainer

onready var game_vp = $Viewport

var base_res = Vector2(1280,720)
var game_res = Vector2(320, 180)

func _ready():
	get_tree().root.connect("size_changed", self, "on_size_change")
	game_vp.size_override_stretch = true

func on_size_change():
	game_vp.set_size_override(true, Vector2(320, 180))
