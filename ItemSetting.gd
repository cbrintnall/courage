extends Node2D

onready var data = $"/root/Data"
onready var sprite = $Sprite

var _item = null

func _ready():
	clear_item()

func get_item():
	return _item

func set_item(item):
	_item = item
	
	sprite.texture = AtlasTexture.new()
	sprite.texture.atlas = data.item_texture
	sprite.texture.region = item["atlas_region"]
	
	$"/root/Events".emit_signal("ItemPlaced", self)

func clear_item():
	sprite.texture = null
	_item = null

func is_open():
	return _item == null
