extends CanvasLayer

var _card = preload("res://Card.tscn")

onready var cards = $MarginContainer/VBoxContainer/HBoxContainer
onready var data = $"/root/Data"

func _ready():
	$"/root/Events".connect("NewCardInDeck", self, "on_new_card")
	$"/root/Events".connect("NewTurn", self, "on_new_turn")
	
	$MarginContainer2/HBoxContainer/end_turn.connect("pressed", self, "end_turn")

func end_turn():
	$"/root/Events".emit_signal("EndTurn")

func on_new_turn():
	for child in cards.get_children():
		child.queue_free()

func on_new_card(card_data):
	var card = _card.instance()
	cards.add_child(card)
	card.set_card_data(data.items, card_data)
