extends PathFollow2D

const move_time = 1.0

onready var data = $"/root/Data"

var _satisfied := false
var _want = null
var _is_front = false

func _ready():
	$"/root/Events".connect("ItemPlaced", self, "container_has_item")
	$Tween.connect("tween_all_completed", self, "_on_finished_moving")

func move_to_offset(where: float, delay: float):
	$Tween.interpolate_property(
		self,
		"offset",
		null,
		where,
		move_time,
		Tween.TRANS_CUBIC,
		Tween.EASE_OUT,
		delay
	)
	
	$Tween.start()
	
func _on_finished_moving():
	var wanted_item = _want.get("item", {"name": "_"})
	var item_place = data.item_positions.get_item(wanted_item)
	if item_place and container_has_item(item_place):
		satisfy_want(item_place)

func is_front():
	var parent = get_parent()
	
	return parent is Path2D and parent.curve.get_baked_length() == offset

func container_has_item(item_container):
	var item = item_container.get_item()
	if item["name"] == _want.get("item", {}).get("name") and is_front():
		satisfy_want(item_container)
		
func satisfy_want(item_container):
	if _satisfied:
		return
	
	_satisfied = true
	
	_parent_to_root()
	
	var final_tween = Tween.new()
	add_child(final_tween)
	final_tween.interpolate_property(
		self,
		"global_position",
		null,
		item_container.global_position,
		1.0,
		Tween.TRANS_CUBIC,
		Tween.EASE_OUT
	)
	final_tween.start()
	yield(final_tween,"tween_all_completed")
	item_container.clear_item()
	leave()

func _parent_to_root():
	var parent = get_parent()
	var tree = get_viewport()
	var gpos = global_position
	
	parent.remove_child(self)
	tree.add_child(self)
	global_position = gpos

func leave():
	# hide the sprite, want is satisfied
	$Sprite.visible = false
	
	if get_parent() is Path2D:
		_parent_to_root()

	$"/root/Events".emit_signal("WispLeft", self)
	$Tween.interpolate_property(
		self,
		"global_position",
		null,
		Vector2(data.exit_point.global_position.x, global_position.y),
		data.wisp_leave_time,
		Tween.TRANS_CUBIC,
		Tween.EASE_OUT
	)
	$Tween.start()
	yield($Tween, "tween_all_completed")
	queue_free()

func set_want(data: Dictionary):
	$Sprite.texture = AtlasTexture.new()
	$Sprite.texture.atlas = load("res://art/items.png")
	$Sprite.texture.region = data["item"]["atlas_region"]
	
	_want = data
