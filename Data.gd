extends Node

const wisp_line_distance = 20.0
const wisp_leave_time = 3.0

var item_texture = load("res://art/items.png")
var exit_point: Node2D = null
var item_positions = null

var items = {
	"anti-matter": {
#		"symbol": Texture.new(),
		"name": "Anti-Matter Latte",
		"atlas_region": Rect2(0, 0, 24, 24),
		"value": 4
	}
}

var possible_cards = [
	{ "produces": "anti-matter", "amount": 1, "energy": 1 }
]

# things that can happen each turn
var events = [
	{
		"text": "",
		"item": items["anti-matter"]
	}
]
